package main

import (
	"log"
	"os"

	"gitlab.com/tylerjkatz/httpstatspan/httpstatspan"
)

//ReqURL is a user input convenience const string used in call to Run below
const ReqURL = "https://gitlab.com"

//Span is a user input convenience const int (seconds) used in call to Run below
const Span = 300

//Buffer is a user input convenience const int (seconds) used in call to Run below
const Buffer = 1

func main() {
	// This command runs httpstatspan over a given time, printing result JSON string
	result, err := httpstatspan.Run(ReqURL, Span, Buffer)
	if err != nil {
		log.Panicf("[ERROR] - %v", err)
	}

	// write json to file (it can get large)
	f, err := os.Create("report.json")
	if err != nil {
		panic(err)
	}
	defer f.Close()
	f.WriteString(result)

	//DEBUG
	//fmt.Printf("Raw JSON results (ms):\n%s", result)
}
