#!/bin/bash
#
# Code coverage

#COVERAGE_DIR="${COVERAGE_DIR:-coverage}"
COVERAGE_DIR="coverage"
package1="gitlab.com/tylerjkatz/httpstatspan/httpstatspan"

# Create the coverage files directory
mkdir -p "$COVERAGE_DIR" || true;
cd "$COVERAGE_DIR"

# Create html report for httpstatspan
echo ${package1##*/} ;
go test -coverprofile "${package1##*/}.cov" "$package1" && \
go tool cover -func="${package1##*/}.cov" && \
go tool cover -html="${package1##*/}.cov" -o "${package1##*/}-coverage.html" || true ;