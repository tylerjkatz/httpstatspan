/*Package httpstatspan features a single exported function, Run, that runs a series of http requests
over a span of time, retrieves response times, does some basic statistical analysis, and returns
results via JSON.  The structure of the returned JSON is embodied by the exported type ResultsJSON.*/
package httpstatspan

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"time"

	"github.com/tcnksm/go-httpstat"
)

// ResultsJSON is a struct representing JSON returned by Start
type ResultsJSON struct {
	// Logs is an array of strings representing request logs from Run
	Logs []string

	// Headers is an array of strings representing names for each element in result arrays
	Headers []string

	// Data is an array of field names for each inner result array element
	Data [][]float32

	// Overall is an array of field names for each Overall map value
	Overall map[string][]float32
}

// Run executes http requests to a given url over span of time, returning stats as JSON string
func Run(reqURL string, span int, buffer int) (string, error) {
	// set log prefix
	log.SetPrefix("httpstatspan : ")

	// validation - basic checks to ensure reqURL is valid
	_, err := url.ParseRequestURI(reqURL)
	isMatch, _ := regexp.MatchString(`//.+\..+`, reqURL)

	if err != nil || isMatch == false {
		log.Printf(": [ERROR] - invalid url!")
		return "", errors.New("invalid url")
	}

	// validation - ensure span is greater than zero
	if span <= 0 {
		log.Printf(": [ERROR] - span must be a positive integer (seconds)!")
		return "", errors.New("span must be a positive integer (seconds)")
	}

	// validation - ensure buffer is not negative
	if buffer < 0 {
		log.Printf(": [ERROR] - buffer cannot be negative!")
		return "", errors.New("buffer cannot be negative")
	}

	// results base struct for returned JSON
	spanResults := ResultsJSON{
		Logs:    []string{},
		Headers: []string{"dns", "tcp", "tls", "server", "content", "total"},
		Data:    [][]float32{},
		Overall: map[string][]float32{
			"high":     {},
			"avg":      {},
			"low":      {},
			"runtime":  {},
			"requests": {},
		},
	}

	// log start
	log.Printf(": [OUT] - httpstatspan run started %s", time.Now().Format(time.StampMilli))
	log.Printf(": [OUT] - Arguments [ ReqURL: %s , Span: %ds , Buffer %ds ]", reqURL, span, buffer)
	log.Printf(": [OUT] - Response timing headers  ---------------------------- %v", spanResults.Headers)
	spanResults.Logs = append(spanResults.Logs, fmt.Sprintf("%s : [OUT] - httpstatspan run started %s", time.Now().Format(time.StampMilli), time.Now().Format(time.StampMilli)))
	spanResults.Logs = append(spanResults.Logs, fmt.Sprintf("%s : [OUT] - Arguments [ ReqURL: %s , Span: %ds , Buffer %ds ]", time.Now().Format(time.StampMilli), reqURL, span, buffer))
	spanResults.Logs = append(spanResults.Logs, fmt.Sprintf("%s : [OUT] - Response timing headers ---------------------------- %v", time.Now().Format(time.StampMilli), spanResults.Headers))

	// set start time of the run
	start := time.Now()

	// set an end time to mark termination of main event loop
	end := start.Add(time.Duration(span) * time.Second)

	// main loop that calls doRequest and prints a timestamp per call
	for time.Now().Before(end) {
		res, err := doRequest(reqURL, spanResults)
		if err != nil {
			log.Println(err)
			continue
		}

		// update spanResults
		spanResults = res

		// wait for buffer time to elapse
		time.Sleep(time.Duration(buffer) * time.Second)
	}

	// get actual time of span
	stop, spanDuration := time.Now(), time.Since(start)

	// append runtime and number of requests
	spanResults.Overall["runtime"] = append(spanResults.Overall["runtime"], float32(spanDuration/time.Millisecond))
	spanResults.Overall["requests"] = append(spanResults.Overall["requests"], float32(len(spanResults.Data)))

	// log end of httpstatspan and timestamp
	log.Printf(": [OUT] - httpstatspan ended at %s", stop.Format(time.StampMilli))
	log.Printf(": [OUT] - TOTAL RUNTIME: %vms", int(spanDuration/time.Millisecond))
	spanResults.Logs = append(spanResults.Logs, fmt.Sprintf("%s : [OUT] - httpstatspan ended at %s", time.Now().Format(time.StampMilli), stop.Format(time.StampMilli)))
	spanResults.Logs = append(spanResults.Logs, fmt.Sprintf("%s : [OUT] - TOTAL RUNTIME: %vms", time.Now().Format(time.StampMilli), int(spanDuration/time.Millisecond)))

	// error if no results
	if len(spanResults.Data) == 0 {
		log.Printf(": [ERROR] - no results were found!")
		return "", errors.New("no results were found")
	}

	//return JSON as string
	jsonBytes, err := json.MarshalIndent(spanResults, "", "    ")
	if err != nil {
		log.Printf(": [ERROR] - unable to marshal spanResults to JSON!")
		return "", errors.New("unable to marshal spanResults to json")
	}
	return string(jsonBytes), nil
}

// doRequest performs the request on the passed reqURL, recording results to a ResultsJSON struct
func doRequest(reqURL string, results ResultsJSON) (ResultsJSON, error) {
	// Create a new HTTP request
	req, err := http.NewRequest("GET", reqURL, nil)
	if err != nil {
		log.Println(err)
		return results, errors.New("could not create new request")
	}

	// Recreate req with httpstat context
	var result httpstat.Result
	ctx := httpstat.WithHTTPStat(req.Context(), &result)
	req = req.WithContext(ctx)

	// Send request via default http client
	client := http.DefaultClient
	res, err := client.Do(req)
	if err != nil {
		log.Println(err)
		return results, errors.New("could not execute request")
	}

	// discard/close response
	if _, err := io.Copy(ioutil.Discard, res.Body); err != nil {
		log.Println(err)
		return results, errors.New("io error occurred")
	}
	res.Body.Close()

	// set end time to compute end of content transfer time
	end := time.Now()

	// assign component timing
	dns := float32(result.DNSLookup / time.Millisecond)
	tcp := float32(result.TCPConnection / time.Millisecond)
	tls := float32(result.TLSHandshake / time.Millisecond)
	server := float32(result.ServerProcessing / time.Millisecond)
	content := float32(result.ContentTransfer(end) / time.Millisecond)
	total := dns + tcp + tls + server + content

	// assign slice to add to results.Data
	reqResults := []float32{dns, tcp, tls, server, content, total}

	// log results of this request run
	results.Data = append(results.Data, reqResults)
	log.Printf(": [OUT] - Request #%v completed at %v **RESULTS** %v", len(results.Data), end.Format(time.StampMilli), reqResults)
	results.Logs = append(results.Logs, fmt.Sprintf("%s : [OUT] - Request #%v completed at %v **RESULTS** %v", time.Now().Format(time.StampMilli), len(results.Data), end.Format(time.StampMilli), reqResults))

	// calculate overall stats
	for r, ms := range reqResults {
		// expand overall as needed
		if len(results.Overall["high"]) < r+1 {
			results.Overall["high"] = append(results.Overall["high"], 0)
			results.Overall["avg"] = append(results.Overall["avg"], 0)
			results.Overall["low"] = append(results.Overall["low"], 0)
		}

		// -- high calculation
		if len(results.Data) == 1 || ms > results.Overall["high"][r] {
			results.Overall["high"][r] = ms
		}

		// -- avg calculation
		results.Overall["avg"][r] = (ms + results.Overall["avg"][r]*float32(len(results.Data)-1)) / (float32(len(results.Data)-1) + 1)

		// -- low calculation
		if len(results.Data) == 1 || ms < results.Overall["low"][r] {
			results.Overall["low"][r] = ms
		}

		//DEBUG
		//log.Printf(": [OUT] - results length: %v, x %v, y %v, this span %v", len(results), x, y, results[y])
	}

	// return the results object to update spanResults
	return results, nil
}
