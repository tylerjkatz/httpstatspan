package httpstatspan_test

import (
	"encoding/json"
	"log"
	"testing"

	"gitlab.com/tylerjkatz/httpstatspan/httpstatspan"
)

// resultsJSON is a global ResultsJSON reference to share among tests
var resultsJSON httpstatspan.ResultsJSON
var url string
var span int
var buffer int

// TestValidJSON tests that results can be unmarshalled to ResultsJSON struct
func TestValidJSON(t *testing.T) {
	url, span, buffer := "https://gitlab.com", 5, 0

	got, err := httpstatspan.Run(url, span, buffer)
	if err != nil {
		t.Errorf("[ERROR] - %v", err)
	}

	if err := json.Unmarshal([]byte(got), &resultsJSON); err != nil {
		t.Errorf("Run(\"%s\", %d, %d) = INVALID JSON", url, span, buffer)
	}

	log.Printf("JSON output:\n%s", got)
}

// TestDataLengths ensures all slices in Data have length equal to number of Headers
func TestDataLengths(t *testing.T) {
	url, span, buffer := "https://gitlab.com", 5, 0
	count := len(resultsJSON.Headers)

	for i, row := range resultsJSON.Data {
		if len(row) != count {
			t.Errorf("Run(\"%s\", %d, %d) => len(resultsJSON.Data[%v]) is %v; want %v", url, span, buffer, i, len(row), count)
		}
	}
}

// TestOverallLengths ensures all slices in Overall have length equal to number of Headers
func TestOverallLengths(t *testing.T) {
	url, span, buffer := "https://gitlab.com", 5, 0
	headers, count := len(resultsJSON.Headers), 0

	for key, row := range resultsJSON.Overall {
		if key == "runtime" || key == "requests" {
			count = 1
		} else {
			count = headers
		}

		if len(row) != count {
			t.Errorf("Run(\"%s\", %d, %d) => len(resultsJSON.Overall[%v]) is %v; want %v", url, span, buffer, key, len(row), count)
		}
	}
}

// TestDataTotalTimes tests that each row of resultsJSON.Data has total that is valid
func TestDataTotalTimes(t *testing.T) {
	url, span, buffer := "https://gitlab.com", 5, 0
	total := 0

	for y := range resultsJSON.Data {
		for x := 0; x < len(resultsJSON.Data[y])-1; x++ {
			total += int(resultsJSON.Data[y][x])
		}
		if float32(total) != resultsJSON.Data[y][5] {
			t.Errorf("Run(\"%s\", %d, %d) => resultsJSON.Data[%v] total time is %v; want %v", url, span, buffer, y, total, resultsJSON.Data[y][5])
		}
		total = 0
	}
}

// TestHighs tests that all highs are true highs
func TestHighs(t *testing.T) {
	url, span, buffer := "https://gitlab.com", 5, 0
	for y := range resultsJSON.Data {
		for x, val := range resultsJSON.Data[y] {
			if resultsJSON.Data[y][x] > resultsJSON.Overall["high"][x] {
				t.Errorf("Run(\"%s\", %d, %d) => resultsJSON.Data[%v][%v] = %v but high is %v", url, span, buffer, y, x, val, resultsJSON.Overall["high"][x])
			}
		}
	}
}

// TestAvgs tests that all avgs are true avgs
func TestAvgs(t *testing.T) {
	url, span, buffer := "https://gitlab.com", 5, 0
	total := float32(0)

	for x := 0; x < len(resultsJSON.Headers); x++ {
		for y := range resultsJSON.Data {
			total += resultsJSON.Data[y][x]
		}
		avg := float32(total) / float32(len(resultsJSON.Data))

		//check to see if values are close enough (within 0.01ms)
		if avg-resultsJSON.Overall["avg"][x] > 0.01 {
			t.Errorf("Run(\"%s\", %d, %d) => avg for %s=%v; want %v", url, span, buffer, resultsJSON.Headers[x], avg, resultsJSON.Overall["avg"][x])
		}
		total = float32(0)
	}
}

// TestLows tests that all lows are true lows
func TestLows(t *testing.T) {
	url, span, buffer := "https://gitlab.com", 5, 0
	for y := range resultsJSON.Data {
		for x, val := range resultsJSON.Data[y] {
			if resultsJSON.Data[y][x] < resultsJSON.Overall["low"][x] {
				t.Errorf("Run(\"%s\", %d, %d) => resultsJSON.Data[%v][%v] = %v but low is %v", url, span, buffer, y, x, val, resultsJSON.Overall["low"][x])
			}
		}
	}
}

// TestRuntime ensures that total run time is accurate per span (within standard error)
func TestRuntime(t *testing.T) {
	url, span, buffer := "https://gitlab.com", 5, 0

	// Runtime should be >= span and <= span + last request total + buffer + 100ms (standard error)
	runtime := int(resultsJSON.Overall["runtime"][0])
	min := span * 1000
	max := span*1000 + buffer*1000 + int(resultsJSON.Data[len(resultsJSON.Data)-1][5]) + 100
	if runtime < min || runtime > max {
		t.Errorf("Run(\"%s\", %d, %d) => runtime = %v, want %v to %v", url, span, buffer, runtime, min, max)
	}
}

//TestRequestCount tests that request count is accurrate to Data
func TestRequestCount(t *testing.T) {
	url, span, buffer := "https://gitlab.com", 5, 0

	requests := int(resultsJSON.Overall["requests"][0])
	if requests != len(resultsJSON.Data) {
		t.Errorf("Run(\"%s\", %d, %d) => request count = %v, want %v", url, span, buffer, requests, len(resultsJSON.Data))
	}
}

//TestInvalidURL tests that an err occurs when url is invalid
func TestInvalidURL(t *testing.T) {
	//BEGIN error flows
	log.Println(": BEGIN ERROR FLOW OUTPUT")

	url, span, buffer := "https//gitlab.com", 5, 0

	_, err := httpstatspan.Run(url, span, buffer)
	if err == nil {
		t.Errorf("Run(\"%s\", %d, %d) => want err", url, span, buffer)
	}

	url, span, buffer = "https://gitlab", 5, 0

	_, err = httpstatspan.Run(url, span, buffer)
	if err == nil {
		t.Errorf("Run(\"%s\", %d, %d) => want err", url, span, buffer)
	}
}

//TestInvalidSpan tests that an err occurs when span is zero or negative
func TestInvalidSpan(t *testing.T) {
	url, span, buffer := "https://gitlab.com", -5, 0

	_, err := httpstatspan.Run(url, span, buffer)
	if err == nil {
		t.Errorf("Run(\"%s\", %d, %d) => want err", url, span, buffer)
	}

	url, span, buffer = "https://gitlab.com", 0, 0

	_, err = httpstatspan.Run(url, span, buffer)
	if err == nil {
		t.Errorf("Run(\"%s\", %d, %d) => want err", url, span, buffer)
	}
}

//TestInvalidBuffer tests that an err occurs when buffer is negative
func TestInvalidBuffer(t *testing.T) {
	url, span, buffer := "https://gitlab.com", 5, -1

	_, err := httpstatspan.Run(url, span, buffer)
	if err == nil {
		t.Errorf("Run(\"%s\", %d, %d) => want err", url, span, buffer)
	}
	//END error flows
	log.Println(": END ERROR FLOW OUTPUT")
	log.Println("***END TESTING***")
}
