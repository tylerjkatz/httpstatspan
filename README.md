# httpstatspan

[![Build Status](https://gitlab.com/tylerjkatz/httpstatspan/badges/master/build.svg)](https://gitlab.com/tylerjkatz/httpstatspan/commits/master) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/tylerjkatz/httpstatspan)](https://goreportcard.com/report/gitlab.com/tylerjkatz/httpstatspan) [![Coverage Report](https://gitlab.com/tylerjkatz/httpstatspan/badges/master/coverage.svg)](https://gitlab.com/tylerjkatz/httpstatspan/commits/master) [![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

**httpstatspan** is a Golang package that features a single exported function, `Run`, that executes a series of http requests over a span of time (seconds), retrieves response times, does some basic statistical analysis, and returns results via JSON string.  The structure of the returned JSON is embodied by the exported type `httpstatspan.ResultsJSON`.

### Execution

From repo root:

build

```
go build
```

run (main)
```
go run httpstatspan_main.go
```

This will build the project and execute `httpstatspan_main.go` which contains the `main` function and calls `Run`, an exported function from package `httpstatspan`.  `Run` returns a stringified JSON response which is written to the local directory as `report.json`.  Logs per request are included in the JSON for convenience.

The full signature of function `Run` is `httpstatspan.Run(ReqURL string, Span int, Buffer int)`.  `Run` executes http requests to a given url over span of time and includes the response times in the returned JSON string.

The response times are broken down into 5 component times plus the total time: dns, tcp, tls, server, content, and their total.  All 6 of these measurements (including the total response time) are processed to determine additional overall statistics.  Overall statistics include average, high, and low values for each of the 6 measurements. 

Easily change the parameters to test `httpstatspan.Run` by editing the constants at the top of `httpstatspan_main.go` and rebuilding/executing.

Windows executable `httpstatspan.exe` has been added to the repo with default values: url of 'https://gitlab.com/', span length of 300 seconds (5 minutes), and buffer length of 1 second.


## Author

* **Tyler Katz**  [TJK](https://gitlab.com/tylerjkatz)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [Pantomath](https://about.gitlab.com/2017/11/27/go-tools-and-gitlab-how-to-do-continuous-integration-like-a-boss/)
* [go-httpstat](https://github.com/tcnksm/go-httpstat)