package hello

//Greet returns a hearty hello in a given language!
func Greet(lang string) string {
	if lang == "English" {
		return "Howdy!"
	} else if lang == "Japanese" {
		return "Konnichi Wa!"
	} else {
		return "..smile and nod.."
	}
}
