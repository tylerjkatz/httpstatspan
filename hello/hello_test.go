package hello_test

import (
	"fmt"
	"testing"

	"gitlab.com/tylerjkatz/httpstatspan/hello"
)

func testGreet(tt *testing.T, in string, want string) {
	got := hello.Greet(in)

	fmt.Printf("Greeting: %s\n", got)

	if want != got {
		tt.Errorf("Greet(\"%s\") = \"%s\"; want \"%s\"", in, got, want)
	}
}

func TestEnglish(t *testing.T)  { testGreet(t, "English", "Howdy!") }
func TestJapanese(t *testing.T) { testGreet(t, "Japanese", "Konnichi Wa!") }

func TestGibberish(t *testing.T) { testGreet(t, "Gibberish", "..smile and nod..") }
